const mongoose= require('mongoose');
// const { INTEGER } = require('sequelize/types');

const Schema = mongoose.Schema;

const githubSchema= new Schema({
    id:{
        type:Number,
        required: true, unique : true
    },
    userid:{
        type:String,
        required: true, 
        unique : true
    }, repo_name:[{
        type:String,
        required: true
    }], commit_url:[{
        type:String,
        required: true
    }],
    //  location:{
    //     type:String,
    //     required: true
    // }, description:{
    //     type:String,
    //     required: true
    // }
})

module.exports=mongoose.model('RepoCommit',githubSchema)