## Introduction
This is the website that built using MERN-Stack. It can access the GitHub user-id then returns Repos and Commits information. we don't need to fetch Github API every time to get Repos and Commits. we can retrieve the information from the Cloud(Mongo Atlas)

### Flow Diagram
 gitHuBfInder.pdf 

## Usage 💻
REST API Sample Usage

userid should be username of github. here userid is rajac16075

https://api.github.com/users/rajac16075/repos?per_page=150

https://api.github.com/users/${userId}/repos?per_page=150 (Internal Api call for getting the data from GitHub server )


### To run the server locally on your machine:
git clone https://gitlab.com/rajac16075/Hopstack

cd Github-Finder 

nodemon App.js (For server)

cd client

npm start (For client)





