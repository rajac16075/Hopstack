import './App.css';
import React, { useState} from "react";
import { Slide } from "react-awesome-reveal";


 const App = () => {

  const [response, setResponse] = useState([]);
  const [userId, setUserId] = useState('')
  const [messageDB,setMessageDb]= useState('')
  const [showerr,setShowerr]=useState('')
  const [invalid,setInvalidinfo]=useState('')
  const [success,setSuccess]=useState('')
  
  const pressenter=(e)=>{
    if(e.key=='Enter'){
      return searchUserid()
    }
  }

  const searchUserid=async()=>{
    setResponse([])
    setInvalidinfo('')
    setMessageDb('')
    setShowerr('')
    setSuccess('')
    if(userId!==''){
    console.log(userId,'sdfg')
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
      'Accept': 'application/json' },
      body: JSON.stringify({ id: userId })
  };
  console.log('we',requestOptions)
  const resp= await fetch('/check', requestOptions)
   const dat=await resp.json()
   console.log(dat,'lp')
   if(dat!='no_user'){
   var loc=[]
   var repo=dat.repo_name
   var commit=dat.commit_url
   repo.map((i,index)=>{
     loc[index]={
       name: i,
       commits_url:commit[index]
     }
   })
   setResponse(loc)
  
  setMessageDb('DB')
  console.log(loc,'dsfa')
   }
  else{

    const response= await fetch(`https://api.github.com/users/${userId}/repos?per_page=150`,{
      method:'GET'
    })
    
    const data=await response.json()
   
    console.log(data,'ppp')
    
    //    data =>{
    //   // =console.log(data[2].name,'ppp')
    if(data.length>0){
      setResponse(data)
      var repo=[]
    var commitsarray=[]
    // console.log('hellllloo', userid,id)
    data.map(i=>{
        //  console.log(i.id,'vareva')
    repo.push(i.name)
    commitsarray.push(i.commits_url)
    })
    // console.log('hellllloo',commitsarray,repo)
      const requestOpt = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
        'Accept': 'application/json' },
        body: JSON.stringify({ repo:repo,commitsarray:commitsarray,userid: userId ,id:data[0].id })
    };
      const store= await fetch('/store', requestOpt)
      // .then(response => response.json())
      // .then(data => console.log(data,'lp'));
   const rep=await store.json()
   if(rep=='Success'){
     setSuccess('success')
   }
   console.log(rep,'lpp')
     }
     else{
       setResponse([])
       setShowerr('err')
     }
    // }
  }

  }
  else{
    if(userId==''){
      setInvalidinfo('err')
    }
  }
  
}
  

   return (
    <Slide triggerOnce>
     <div className='sud' style={{margin:'auto',marginTop:'12vw',textAlign:'center  '}}>

<h2 style={{color:'whitesmoke'}} class="display-4">Github-Finder</h2>


       <span>
        {showerr == 'err' ? <Slide triggerOnce>
        
        <section>

  <div className="container mt-5">
    <div className="row">

      <div className="col-sm-12" style={{textAlign:'center'}}>
        <div className="alert fade alert-simple alert-danger alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show" role="alert" data-brk-library="component__alert">
          <button type="button" className="close font__size-18" data-dismiss="alert">
									<span aria-hidden="true">
										{/* <i className="fa fa-times danger "></i> */}
									</span>
									<span className="sr-only">Close</span>
								</button>
          <i className="start-icon far fa-times-circle faa-pulse animated"></i>
          <strong  className="font__weight-semibold">Oh snap!</strong> No User or User is private
        </div>
      </div>

    </div>
  </div>
</section>
</Slide>

         
       :''}
       {invalid == 'err' ? <Slide triggerOnce><section>

<div className="container mt-5">
  <div className="row">

  

  <div className="col-sm-12">
        <div className="alert fade alert-simple alert-warning alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show" role="alert" data-brk-library="component__alert">
          <button type="button" className="close font__size-18" data-dismiss="alert">
									<span aria-hidden="true">
										{/* <i className="fa fa-times warning"></i> */}
									</span>
									<span className="sr-only">Close</span>
								</button>
          <i className="start-icon fa fa-exclamation-triangle faa-flash animated"></i>
          <strong  className="font__weight-semibold">Warning!</strong>  Please enter the UserId
        </div>
      </div>

  </div>
</div>
</section>

     </Slide>   
       :''}
       

       {success == 'success' ? <Slide triggerOnce><section>

<div className="container mt-5">
  <div className="row">

  



  <div className="col-sm-12" style={{textAlign:'center'}}>
        <div className="alert fade alert-simple alert-success alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show">
          <button type="button" className="close font__size-18" data-dismiss="alert">
									<span aria-hidden="true"><a href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjVtMfrsbLwAhXryzgGHdXMB7I4ChAWMAh6BAgIEAM&url=https%3A%2F%2Fwww.ftcab.com%2F&usg=AOvVaw3v44T5yZdpdgVq0agYf0eg" target="_blank">
                    {/* <i className="fa fa-times greencross"></i> */}
                    </a></span>
									<span className="sr-only">Close</span> 
								</button>
          <i className="start-icon far fa-check-circle faa-tada animated"></i>
          <strong   className="font__weight-semibold">Well done!</strong> Successfully stored in the Database.
        </div>
      </div>

  </div>
</div>
</section></Slide> :''}



       {messageDB == 'DB' ?<Slide triggerOnce><section>

<div className="container mt-5">
  <div className="row">

  <div className="col-sm-12" style={{textAlign:'center'}}>
        <div className="alert fade alert-simple alert-primary alert-dismissible text-left font__family-montserrat font__size-16 font__weight-light brk-library-rendered rendered show" role="alert" data-brk-library="component__alert">
          <button type="button" className="close font__size-18" data-dismiss="alert">
									<span  aria-hidden="true">
                    {/* <i className="fa fa-times alertprimary"></i> */}
                    </span>
									<span className="sr-only">Close</span>
								</button>
          <i className="start-icon fa fa-thumbs-up faa-bounce animated"></i>
          <strong  className="font__weight-semibold">Well done!</strong> User already in the Database
        </div>
</div>

  </div>
</div>
</section>
</Slide> 
 :''}
       </span>
 <div className="md-form " style={{textAlign:'center'}} > 
   
      {/* <label style={{color:'', fontSize:'1vw' ,textAlign:'center',fontWeight:'bold', paddingTop:'0.5vw', marginRight:'1vw'}} htmlFor="form2">Username</label> */}
    <input style={{paddingLeft:'-35vw', height:'auto',borderRadius:'23px', marginRight:'0.7vw'}} placeholder='Enter Github Username...' type="search"  id="form2" onKeyDown={pressenter} onChange={e=>{
      setInvalidinfo('')
      setUserId(e.target.value)}} className="form-control form-control-lg"/>
    <br/>
    <button type="button"  onClick={searchUserid} className="btn ">Go!</button>


</div>
<br/>

{
response.length >0 ?  <table className="table align-middle">
<thead>
  <tr>
    <th scope="col">#</th>
    <th scope="col">REPOSITORIES</th>
    <th scope="col">COMMITS</th>

  </tr>
</thead>


{
        response.length >0 ? response.map((i,index) =>
   
  <tbody key={index}>
    <tr>
      <th scope="row">{index+1}</th>
      <td>{i.name}</td>
      <td>
        {/* <a>{i.commits_url.slice(0,-6)}</a> */}
        <a style={{height:'100%',width:'100%',fontWeight:'normal'}} href={`${i.commits_url.slice(0,-6)}`}target='_blank'>
    <div style={{height:'100%',width:'100%'}}>
    {i.commits_url.slice(0,-6)}
    </div>
  </a>
        </td>
      {/* <td>@mdo</td> */}
    </tr>

  </tbody> ) : ''
      }
      </table>:''
    }
     </div>
     </Slide>
   
   )
 }


 export default App
